# chenyu-sys

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test

# npm install vuex --save

# npm install axios --save

#  npm i element-ui -S
 import ElementUI from 'element-ui';
 import 'element-ui/lib/theme-chalk/index.css';
  Vue.use(ElementUI);
 # 按需加载
  npm install babel-plugin-component -D
#.babelrc 里面
  "plugins": ["transform-vue-jsx", "transform-runtime",[
    "component",
    {
      "libraryName": "element-ui",
      "styleLibraryName": "theme-chalk"
    }
  ]]
npm install moment --save
```
### 跨域处理
1. 生产环境
2. 开发环境
"/api":{
    target:"http://tingapi.ting.baidu.com",
    pathRewrite:{
        '^/api': ''
    },
    changeOrigin:true
}

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).



C:\Users\Administrator\Desktop\chenyu-sys\config\index.js
设置  productionSourceMap: false, 可减小文件大小
安装
npm install --save-dev compression-webpack-plugin@1.1.12  记得带版本号
更改 C:\Users\Administrator\Desktop\chenyu-sys\build\webpack.prod.conf.js 
      // asset: '[path].gz[query]', 改为  filename: '[path].gz[query]',
    CompressionWebpackPlugin, 开启gzip 压缩
    
    nginx 开启gzip
    gzip on;
    gzip_static on;
    gzip_buffers 4 16k;
    gzip_comp_level 5;
    gzip_types text/plain application/javascript text/css application/xml text/javascript application/x-httpd-php image/jpeg
               image/gif image/png;

1.考前睡好，早饭吃饱，谨慎喝水（排的晚，要等到11点，肚里有粮，心里不慌）
2.一定一定要穿的暖暖和和（不然本来就紧张抖，加上外面冻得更抖）
3.大家都紧张，紧张不是坏事（多做深呼吸）
