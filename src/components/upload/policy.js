import api from '@/api'

export function getOssPolicy () {
  return new Promise((resolve, reject) => {
    api.getOssPolicy().then(({data}) => {
      resolve(data)
    })
  })
}
