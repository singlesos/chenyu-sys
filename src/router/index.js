/*
 * @Author: your name
 * @Date: 2021-03-18 21:08:18
 * @LastEditTime: 2021-06-07 21:58:39
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /chenyu-sys/src/router/index.js
 */
import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/pages/home'
import Login from '@/pages/Login'
import Index from '@/pages/Index'
import Word from '@/pages/Word'
// import PublicNumber from '@/pages/PublicNumber'
Vue.use(Router)

const originalPush = Router.prototype.push

Router.prototype.push = function push (location) {
  return originalPush.call(this, location).catch(err => err)
}

export default new Router({
  routes: [
    {
      path: '/',
      component: Home, // Home 变为公众号
      redirect: '/index', // 2021-06-07 fix:在此处加一个重定向  即可实现 页面加载初次跳转
      children: [
        {
          path: '/index',
          name: 'Index',
          component: Word,
          meta: {
            isLogin: false
          }
        },
        {
          path: '/singer',
          name: 'singer',
          component: resolve =>
            require(['@/pages/Music/Singer/singer'], resolve),
          meta: {
            isLogin: true
          }
        },
        {
          path: '/song',
          name: 'song',
          component: resolve => require(['@/pages/Music/Song/song'], resolve),
          meta: {
            isLogin: true
          }
        },
        {
          path: '/songList',
          name: 'songList',
          component: resolve => require(['@/pages/Music/SongList/songList'], resolve),
          meta: {
            isLogin: true
          }
        },
        {
          path: '/songListDetail',
          name: 'songListDetail',
          component: resolve => require(['@/pages/Music/SongList/songListDetail'], resolve),
          meta: {
            isLogin: true
          }
        },
        {
          path: '/user',
          name: 'user',
          component: resolve => require(['@/pages/Admin/User/user'], resolve),
          meta: {
            isLogin: true
          }
        },
        {
          path: '/role',
          name: 'role',
          component: resolve => require(['@/pages/Admin/Role/role'], resolve),
          meta: {
            isLogin: true
          }
        },
        {
          path: '/permission',
          name: 'permission',
          component: resolve =>
            require(['@/pages/Admin/Permission/permission'], resolve),
          meta: {
            isLogin: true
          }
        }
      ]
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    }
  ]
})
