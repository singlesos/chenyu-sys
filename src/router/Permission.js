import router from './index'
import store from '@/store'

/* 权限处理 */
router.beforeEach((to, from, next) => {
  if (to.meta.isLogin) {
    // 读取token
    const token = store.state.userInfo
    console.log(token)
    if (token) {
      next()
    } else {
      next({
        path: '/login'
      })
    }
  }
  next()
})
