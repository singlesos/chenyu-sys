import axios from '../utils/http'
import base from './base'

const api = {
  login (params) {
    /**
     * axios.post(url,{username:'',password:''})
     */
    console.log(base.baseUrl + base.login)
    return axios.post(base.baseUrl + base.login, params)
  },
  // 歌手相关接口
  addSinger (params) {
    return axios.post(base.baseUrl + base.addSinger, params)
  },
  updateSinger (params) {
    return axios.put(base.baseUrl + base.updateSinger, params)
  },
  getSingerByPage (params) {
    console.log('getSingerByPage')
    console.log(params)
    return axios.get(base.baseUrl + base.getSingerByPage, params)
  },
  getSingerList (params) {
    console.log('getSingerByList')
    console.log(params)
    return axios.get(base.baseUrl + base.getSingerList, params)
  },
  getSingerById (params) {
    console.log('getSingerById')
    console.log(params)
    return axios.get(base.baseUrl + base.getSingerById, params)
  },
  // 歌曲相关接口
  addSong (params) {
    return axios.post(base.baseUrl + base.addSong, params)
  },
  updateSong (params) {
    return axios.put(base.baseUrl + base.updateSong, params)
  },
  getSongByPage (params) {
    console.log('getSongByPage')
    console.log(params)
    return axios.get(base.baseUrl + base.getSongByPage, params)
  },
  getSongById (params) {
    console.log('getSongById')
    console.log(params)
    return axios.get(base.baseUrl + base.getSongById, params)
  },

  getSongNotInSongList (params) {
    console.log('getSongNotInSongList')
    console.log(params)
    return axios.get(base.baseUrl + base.getSongNotInSongList, params)
  },
  getSongInSongList (params) {
    console.log('getSongInSongList')
    console.log(params)
    return axios.get(base.baseUrl + base.getSongInSongList, params)
  },

  // 歌曲相关接口
  addSongList (params) {
    return axios.post(base.baseUrl + base.addSongList, params)
  },
  updateSongList (params) {
    return axios.put(base.baseUrl + base.updateSongList, params)
  },
  getSongListByPage (params) {
    console.log('getSongListByPage')
    console.log(params)
    return axios.get(base.baseUrl + base.getSongListByPage, params)
  },
  saveBatchSongListRelation (params) {
    console.log('saveBatchSongListRelation')
    console.log(params)
    return axios.post(base.baseUrl + base.saveBatchSongListRelation, params)
  },
  deleteSongListRelation (params) {
    console.log('deleteSongListRelation')
    console.log(params)
    return axios.delete(base.baseUrl + base.deleteSongListRelation, params)
  },
  getSongListById (params) {
    console.log('getSongListById')
    console.log(params)
    return axios.get(base.baseUrl + base.getSongListById, params)
  },
  // 文件上传票据
  getOssPolicy () {
    return axios.get(base.baseUrl + base.getOssPolicy, {})
  },
  // 用户相关接口
  addUser (params) {
    return axios.post(base.baseUrl + base.addUser, params)
  },
  removeByIds (params) {
    return axios.delete(base.baseUrl + base.removeByIds, params)
  },
  updateUser (params) {
    return axios.put(base.baseUrl + base.updateUser, params)
  },
  getUserByPage (params) {
    console.log('getUserByPage')
    console.log(params)
    return axios.get(base.baseUrl + base.getUserByPage, params)
  },
  getUserById (params) {
    console.log('getUserById')
    console.log(params)
    return axios.get(base.baseUrl + base.getUserById, params)
  },
  // 角色相关接口
  addRole (params) {
    return axios.post(base.baseUrl + base.addRole, params)
  },
  updateRole (params) {
    return axios.put(base.baseUrl + base.updateRole, params)
  },
  getRoleByPage (params) {
    console.log('getRoleByPage')
    console.log(params)
    return axios.get(base.baseUrl + base.getRoleByPage, params)
  },
  getRoleById (params) {
    console.log('getRoleById')
    console.log(params)
    return axios.get(base.baseUrl + base.getRoleById, params)
  },
  // 权限菜单相关接口
  addPermission (params) {
    return axios.post(base.baseUrl + base.addPermission, params)
  },
  updatePermission (params) {
    return axios.put(base.baseUrl + base.updatePermission, params)
  },
  getPermissionByPage (params) {
    console.log('getPermissionByPage')
    console.log(params)
    return axios.get(base.baseUrl + base.getPermissionByPage, params)
  },
  getPermissionById (params) {
    console.log('getPermissionById')
    console.log(params)
    return axios.get(base.baseUrl + base.getPermissionById, params)
  },
  // 获取二维码列表
  getAllImgList (params) {
    console.log('getAllImgList')
    console.log(params)
    return axios.get(base.getAllImgList, params)
  },
  // 只看未粉
  getNotFanImgListByMyCode (params) {
    console.log('getNotFanImgListByMyCode')
    console.log(params)
    return axios.get(base.getNotFanImgListByMyCode, params)
  },
  // 我粉了的
  getIFanImgListByMyCode (params) {
    console.log('getIFanImgListByMyCode')
    console.log(params)
    return axios.get(base.getIFanImgListByMyCode, params)
  },
  // 粉了我的
  getFanMeImgListByMyCode (params) {
    console.log('getFanMeImgListByMyCode')
    console.log(params)
    return axios.get(base.getFanMeImgListByMyCode, params)
  },
  // 上墙
  addWxCode (params) {
    console.log('addWxCode')
    console.log(params)
    return axios.post(base.addWxCode, params)
  },
  // 粉他
  fanHe (params) {
    console.log('fanHe')
    console.log(params)
    return axios.put(base.fanHe, params)
  },
  // word相关接口
  addWord (params) {
    return axios.post(base.baseUrl + base.addWord, params)
  },
  findWordById (params) {
    console.log('findWordById')
    console.log(params)
    return axios.get(base.baseUrl + base.findWordById, params)
  }
}

export default api
