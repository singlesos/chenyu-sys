import Vue from 'vue'
import Vuex from 'vuex'
import { getToken, removeToken, setUserInfo, getUserInfo, removeUserInfo } from '@/utils/auth'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    userInfo: getUserInfo(),
    token: getToken(),
    roles: [],
    player: {
      audioSongIsPlay: false,
      audioSongUrl: '',
      audioSongId: ''
    }
  },
  getters: {
    audioSongIsPlay: state => state.player.audioSongIsPlay,
    audioSongUrl: state => state.player.audioSongUrl,
    audioSongId: state => state.player.audioSongId
  },
  mutations: {
    setAudioSongIsPlay: (state, audioSongIsPlay) => { state.player.audioSongIsPlay = audioSongIsPlay },
    setAudioSongUrl: (state, audioSongUrl) => { state.player.audioSongUrl = audioSongUrl },
    setAudioSongId: (state, audioSongId) => { state.player.audioSongId = audioSongId },
    SET_USER_INFO: (state, userInfo) => {
      state.userInfo = userInfo
      setUserInfo(userInfo)
    },
    GET_USER_INFO: (state) => {
      state.userInfo = getUserInfo()
    },
    REMOVE_USER_INFO: (state) => {
      state.userInfo = null
      removeUserInfo()
    },
    REMOVE_TOKEN: (state) => {
      state.toekn = null
      removeToken()
    },
    SET_ROLES: (state, roles) => {
      state.roles = roles
    }
  },
  actions: {
    setUserInfoActions  ({commit}, userInfo) {
      commit('SET_USER_INFO', userInfo)
    }
  }
})
