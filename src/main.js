// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import moment from 'moment'
import api from './api'
import store from './store'
import './plugins/element.js'
import './router/Permission'
import VueLocalStorage from 'vue-localstorage'

Vue.use(VueLocalStorage)
// Vue.localStorage.set('someNumber', 123)
// Vue.localStorage.get('someNumber')
Vue.prototype.$api = api
Vue.config.productionTip = false
Vue.prototype.$moment = moment
/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  components: { App },
  template: '<App/>'
})
