const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const backData = require('./backData')

// post传递参数
app.use(bodyParser.urlencoded({
  extended: true
}))

app.use('/api', backData)

app.listen(3000, function () {
  console.log(3000)
})
